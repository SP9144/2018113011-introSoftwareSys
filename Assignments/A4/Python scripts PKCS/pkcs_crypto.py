import random, sys, os
from random import randrange, getrandbits
import random
import sys
DEFAULT_BLOCK_SIZE = 128
BYTE_SIZE = 256


def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a

'''
Euclid's extended algorithm for finding the multiplicative inverse of two numbers
'''
def multiplicative_inverse(e, phi):
    d = 0
    x1 = 0
    x2 = 1
    y1 = 1
    temp_phi = phi
    
    while e > 0:
        temp1 = temp_phi/e
        temp2 = temp_phi - temp1 * e
        temp_phi = e
        e = temp2
        
        x = x2- temp1* x1
        y = d - temp1 * y1
        
        x2 = x1
        x1 = x
        d = y1
        y1 = y
    
    if temp_phi == 1:
        return (d + phi)

def is_prime(n, k=128):
    """ Test if a number is prime

        Args:
            n -- int -- the number to test
            k -- int -- the number of tests to do

        return True if n is prime
    """
    # Test if n is not even.
    # But care, 2 is prime !
    if n == 2 or n == 3:
        return True
    if n <= 1 or n % 2 == 0:
        return False
    # find r and s
    s = 0
    r = n - 1
    while r & 1 == 0:
        s += 1
        r //= 2
    # do k tests
    for _ in range(k):
        a = randrange(2, n - 1)
        x = pow(a, r, n)
        if x != 1 and x != n - 1:
            j = 1
            while j < s and x != n - 1:
                x = pow(x, 2, n)
                if x == 1:
                    return False
                j += 1
            if x != n - 1:
                return False

    return True

def generate_prime_candidate(length):
    """ Generate an odd integer randomly
        Args:
            length -- int -- the length of the number to generate, in bits
        return a integer
    """
    # generate random bits
    p = getrandbits(length)
    # apply a mask to set MSB and LSB to 1
    p |= (1 << length - 1) | 1

    return p

def generate_prime_number(length=1024):
    """ Generate a prime

        Args:
            length -- int -- length of the prime to generate, in          bits

        return a prime
    """
    p = 4
    # keep generating while the primality test fail
    while not is_prime(p, 128):
        p = generate_prime_candidate(length)
    return p

print()

def main():
# create a public/private keypair with 1024 bit keys
    print('Making key files...')
    makeKeyFiles('al_sweigart', 1024)
    print('Key files made.')


def generateKey(keySize):
    # Creates a public/private key pair with keys that are keySize bits in
    # size. This function may take a while to run.
    # Step 1: Create two prime numbers, p and q. Calculate n = p * q.
    print('Generating p prime...')
    p = generate_prime_number(keySize)     
    print('Generating q prime...')
    q = generate_prime_number(keySize) 
    n = p * q
    #print(p, " ", q)
     # Step 2: Create a number e that is relatively prime to (p-1)*(q-1).
    print('Generating e that is relatively prime to (p-1)*(q-1)...')

    while True:
    # Keep trying random numbers for e until one is valid.
         e = random.randrange(2 ** (keySize - 1), 2 ** (keySize))
         if gcd(e, (p - 1) * (q - 1)) == 1:
            break
    
    #print("e: ", e)
    # Step 3: Calculate d, the mod inverse of e.
    print('Calculating d that is mod inverse of e...')
    d = multiplicative_inverse(e, (p - 1) * (q - 1))
    
    publicKey = (n, e)
    privateKey = (n, d)
    
    print('Public key:', publicKey)
    print('Private key:', privateKey)
    
    return (publicKey, privateKey)

def makeKeyFiles(name, keySize):
# Creates two files 'x_pubkey.txt' and 'x_privkey.txt' (where x is the
# value in name) with the the n,e and d,e integers written in them,
# delimited by a comma.
# Our safety check will prevent us from overwriting our old key files:
    if os.path.exists('%s_pubkey.txt' % (name)) or os.path.exists('%s_privkey.txt' % (name)):
         sys.exit('WARNING: The file %s_pubkey.txt or %s_privkey.txt already exists! Use a different name or delete these files and re-run this program.' % (name, name))

    publicKey, privateKey = generateKey(keySize)
    print()
    print('The public key is a %s and a %s digit number.' % (len(str(publicKey[0])), len(str(publicKey[1]))))
    print('Writing public key to file %s_pubkey.txt...' % (name))
    fo = open('%s_pubkey.txt' % (name), 'w')
    fo.write('%s,%s,%s' % (keySize, publicKey[0], publicKey[1]))
    fo.close()
    
    print()
    print('The private key is a %s and a %s digit number.' % (len(str(publicKey[0])), len(str(publicKey[1]))))
    print('Writing private key to file %s_privkey.txt...' % (name))
    fo = open('%s_privkey.txt' % (name), 'w')
    fo.write('%s,%s,%s' % (keySize, privateKey[0], privateKey[1]))
    fo.close()

    filename = 'encrypted_file.txt'
    mode = 'encrypt'
    if mode == 'encrypt':
        message = raw_input("Enetr messgae to be encrypted: ")
        pubKeyFilename = 'al_sweigart_pubkey.txt'
        print('Encrypting and writing to %s...' % (filename))
        encryptedText = encryptAndWriteToFile(filename, pubKeyFilename, message)
        print('Encrypted text:')
        print(encryptedText)
    mode = 'decrypt'
    if mode == 'decrypt':
        privKeyFilename = 'al_sweigart_privkey.txt'
        print('Reading from %s and decrypting...' % (filename))
        decryptedText = readFromFileAndDecrypt(filename, privKeyFilename)
        print('Decrypted text:')
        print(decryptedText)


# If makeRsaKeys.py is run (instead of imported as a module) call
# the main() function.

def getBlocksFromText(message, blockSize=DEFAULT_BLOCK_SIZE):

# Converts a string message to a list of block integers. Each integer

# represents 128 (or whatever blockSize is set to) string characters.

    messageBytes = message.encode('ascii') # convert the string to bytes

    blockInts = []

    for blockStart in range(0, len(messageBytes), blockSize):

    # Calculate the block integer for this block of text

        blockInt = 0

        for i in range(blockStart, min(blockStart + blockSize, len(messageBytes))):

            blockInt += messageBytes[i] * (BYTE_SIZE ** (i % blockSize))

        blockInts.append(blockInt)

    return blockInts

def getTextFromBlocks(blockInts, messageLength, blockSize=DEFAULT_BLOCK_SIZE):

    # Converts a list of block integers to the original message string.

    # The original message length is needed to properly convert the last
    # block integer.

    message = []

    for blockInt in blockInts:

        blockMessage = []

        for i in range(blockSize - 1, -1, -1):

            if len(message) + i < messageLength:

               # Decode the message string for the 128 (or whatever

               # blockSize is set to) characters from this block integer.

               asciiNumber = blockInt // (BYTE_SIZE ** i)

               blockInt = blockInt % (BYTE_SIZE ** i)

               blockMessage.insert(0, chr(asciiNumber))

        message.extend(blockMessage)

    return ''.join(message)

def encryptMessage(message, key, blockSize=DEFAULT_BLOCK_SIZE):

    # Converts the message string into a list of block integers, and then

    # encrypts each block integer. Pass the PUBLIC key to encrypt.

    encryptedBlocks = []

    n, e = key

    for block in getBlocksFromText(message, blockSize):

        # ciphertext = plaintext ^ e mod n

        encryptedBlocks.append(pow(block, e, n))

    return encryptedBlocks

def decryptMessage(encryptedBlocks, messageLength, key, blockSize=DEFAULT_BLOCK_SIZE):

    # Decrypts a list of encrypted block ints into the original message

    # string. The original message length is required to properly decrypt

    # the last block. Be sure to pass the PRIVATE key to decrypt.

    decryptedBlocks = []

    n, d = key

    for block in encryptedBlocks:

       # plaintext = ciphertext ^ d mod n
        decryptedBlocks.append(pow(block, d, n))
    return getTextFromBlocks(decryptedBlocks, messageLength, blockSize)
def readKeyFile(keyFilename):
    # Given the filename of a file that contains a public or private key,

    # return the key as a (n,e) or (n,d) tuple value.
    fo = open(keyFilename)
    content = fo.read()

    fo.close()

    keySize, n, EorD = content.split(',')

    return (int(keySize), int(n), int(EorD))

def encryptAndWriteToFile(messageFilename, keyFilename, message, blockSize=DEFAULT_BLOCK_SIZE):
    # Using a key from a key file, encrypt the message and save it to a

    # file. Returns the encrypted message string.
    keySize, n, e = readKeyFile(keyFilename)
        # Check that key size is greater than block size.

    if keySize < blockSize * 8: # * 8 to convert bytes to bits

        sys.exit('ERROR: Block size is %s bits and key size is %s bits. The RSA cipher requires the block size to be equal to or less than the key size. Either increase the block size or use different keys.' % (blockSize * 8, keySize))

    # Encrypt the message
    encryptedBlocks = encryptMessage(message, (n, e), blockSize)

    # Convert the large int values to one string value.

    for i in range(len(encryptedBlocks)):

        encryptedBlocks[i] = str(encryptedBlocks[i])

    encryptedContent = ','.join(encryptedBlocks)

    # Write out the encrypted string to the output file.

    encryptedContent = '%s_%s_%s' % (len(message), blockSize, encryptedContent)

    fo = open(messageFilename, 'w')

    fo.write(encryptedContent)

    fo.close()

    # Also return the encrypted string.

    return encryptedContent

def readFromFileAndDecrypt(messageFilename, keyFilename):

    # Using a key from a key file, read an encrypted message from a file

    # and then decrypt it. Returns the decrypted message string.

    keySize, n, d = readKeyFile(keyFilename)

    # Read in the message length and the encrypted message from the file.

    fo = open(messageFilename)

    content = fo.read()

    messageLength, blockSize, encryptedMessage = content.split('_')

    messageLength = int(messageLength)

    blockSize = int(blockSize)

    # Check that key size is greater than block size.

    if keySize < blockSize * 8: # * 8 to convert bytes to bits

        sys.exit('ERROR: Block size is %s bits and key size is %s bits. The RSA cipher requires the block size to be equal to or less than the key size. Did you specify the correct key file and encrypted file?' % (blockSize * 8, keySize))

    # Convert the encrypted message into large int values.

    encryptedBlocks = []

    for block in encryptedMessage.split(','):

        encryptedBlocks.append(int(block))

    # Decrypt the large int values.

    return decryptMessage(encryptedBlocks, messageLength, (n, d), blockSize)
    

#if __name__ == '__main__':
#       main()


