import numpy as np

def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a


def multiplicative_inverse(e, phi):
    d = 0
    x1 = 0
    x2 = 1
    y1 = 1
    temp_phi = phi
    
    while e > 0:
        temp1 = temp_phi/e
        temp2 = temp_phi - temp1 * e
        temp_phi = e
        e = temp2
        
        x = x2- temp1* x1
        y = d - temp1 * y1
        
        x2 = x1
        x1 = x
        d = y1
        y1 = y
    
    if temp_phi == 1:
        return d + phi


def is_prime(num):
    if num == 2:
        return True
    if num < 2 or num % 2 == 0:
        return False
    for n in xrange(3, int(num**0.5)+2, 2):
        if num % n == 0:
            return False
    return True

def generate_keypair(p, q):
    if not (is_prime(p) and is_prime(q)):
        raise ValueError('Only Prime allowed.')
    elif p == q:
        raise ValueError('p != q')
    n = p * q

    phi = (p-1)*(q-1)

    e = np.random.randint(1, phi)

    g = gcd(e, phi)
    while g != 1:
        e = np.random.randint(1, phi)
        g = gcd(e, phi)

    d = multiplicative_inverse(e, phi)
    
    return ((e, n), (d, n))

def encrypt(pk, plaintext):
    key, n = pk
    cipher = [(ord(char) ** key) % n for char in plaintext]
    return cipher

def decrypt(pk, ciphertext):
    key, n = pk
    plain = [chr((char ** key) % n) for char in ciphertext]
    return ''.join(plain)
    

def main():
    import random
    primes = []
    for i in range(2, 1000000):
	np.append(primes, i)
    p = random.choice(primes)
    q=0
    while(q==p):
	q = random.choice(primes)
    public, private = generate_keypair(p, q)
    print ("Public key: ", public ,"Private key: ", private)
    message = raw_input("Message to encrypt: ")
    encrypted_msg = encrypt(private, message)
    print ("Encrypted message: ")
    print (''.join(map(lambda x: str(x), encrypted_msg)))
    print ("Message(Decrypted): ")
    print (decrypt(public, encrypted_msg))

main()
