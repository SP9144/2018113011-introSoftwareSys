if ! [ -s list.txt ]
then
    touch list.txt
    echo "SONG ID, SONG NAME, ARTIST NAME, GENRE, LINK" >> list.txt
fi

echo
echo " MENU"
echo " ======================================================================="
echo " 1. Add entry"
echo " 2. Edit entry"
echo " 3. Delete entry"
echo " 4. View all entry"
echo " 5. View selected entries"
echo " 6. Exit"
echo " ======================================================================="
echo
echo -n "--> Pls enter option number: "
read op

case $op in

1) ./Q1-add.sh
    ;;
2) ./Q1-edit.sh
    ;;
3) ./Q1-delete.sh
    ;;
4) ./Q1-view_all.sh
    ;;
5) ./Q1-view_select.sh
    ;;
6)  exit
    ;;
*)  echo
    echo "Pls enter valid option."
    ./Q1-menu.sh
    ;;

esac
