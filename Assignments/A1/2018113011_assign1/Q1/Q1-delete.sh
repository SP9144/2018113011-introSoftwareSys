
echo
echo -n "Enter name of song to be deleted: "
read name
sed -i "/${name}/d" list.txt
echo "Deleted song $name successfully."
echo

echo
echo -n "Do you wish to continue to MENU page (or quit the program)? [Y/n]: "
read ans

if [ "$ans" = "Y" ]
then 
    ./Q1-menu.sh
else
    exit
fi
