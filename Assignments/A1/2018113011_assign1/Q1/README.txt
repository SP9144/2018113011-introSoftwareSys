++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   ISS
ASSIGNMENT 1
-----------------------------------
Shreeya Pahune
2018113011
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

==================================================================

QUESTION 1: Store Music
_________________________

To run script: ./Q1-menu.sh

        This is Menu-driven script which calls other bash scripts to perform the following functions:

        a) Add Entry: Called by ./Q1-add.sh script     
                      Takes input: i) ID
                                   ii) Name
                                   iii) Artist
                                   iv) Genre
                                   v) YouTube Link
                      Appends all of the data provided in an entry to the Music Diary
        
        b) Edit Entry: Called by ./Q1-edit.sh script     
                       Replaces a previous entry with a new one

        c) Delete Entry: Called by ./Q1-delete.sh script     
                         Deletes an entry containing song name given by user

        d) View All Entries: Called by ./Q1-view_all.sh script     
                             Displays all the songs in the Music Diary in a Tabular Format

        e) View Selected Entries: Called by ./Q1-view_selected.sh script     
                                  Displays selected songs in the Music Diary within conditions provided in a Tabular Format
                            
====================================================================

