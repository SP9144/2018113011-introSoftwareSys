
echo -n "Enter Song ID for entry to be replaced: "
read id1
sed -i "/${id1}/d" list.txt

echo
echo "Details of Song no.: $i"
echo
echo -n "Enter Song ID: "
read id
echo -n "Enter Song Name: "
read name
echo -n "Enter Song Artist: "
read artist
echo -n "Enter Song Genre: "
read genre
echo -n "Enter Song YouTube link: "
read link

echo
echo "Your song has been added to your list."
echo "$id, $name, $artist, $genre, $link" >> list.txt


echo
echo -n "Do you wish to continue to MENU page (or quit the program)? [Y/n]: "
read ans

if [ "$ans" = "Y" ]
then 
    ./Q1-menu.sh
else
    exit
fi