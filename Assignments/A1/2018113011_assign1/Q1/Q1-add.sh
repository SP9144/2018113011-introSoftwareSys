echo
echo -n "Enter number of songs that need to be entered: "
read n

for i in $(seq 1 $n);
do
    echo
    echo "Details of Song no.: $i"
    echo
    echo -n "Enter Song ID: "
    read id
    echo -n "Enter Song Name: "
    read name
    echo -n "Enter Song Artist: "
    read artist
    echo -n "Enter Song Genre: "
    read genre
    echo -n "Enter Song YouTube link: "
    read link

    if grep -q $name list.txt; 
    then
        grep $name list.txt
        echo
        echo "Song already exists. Enter another: "
        i=$((i-1))
        continue
    else
        echo
        echo "Your song has been added to your list."
        echo "$id, $name, $artist, $genre, $link" >> list.txt
    fi 

done

echo
echo -n "Do you wish to continue to MENU page (or quit the program)? [Y/n]: "
read ans

if [ "$ans" = "Y" ]
then 
    ./Q1-menu.sh
else
    exit
fi
