echo
echo "All Songs"
echo "=================================================================================================="
echo

awk -F "," '{
    printf "%10s %20s %20s %20s %20s %50s\n", $1, $2, $3, $4, $5, $6
    }' list.txt
echo
echo "=================================================================================================="

echo
echo -n "Do you wish to continue to MENU page (or quit the program)? [Y/n]: "
read ans

if [ "$ans" = "Y" ]
then 
    ./Q1-menu.sh
else
    exit
fi