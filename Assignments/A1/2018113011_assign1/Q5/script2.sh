
# Concatenate 2 files into a single file named target_file.csv
cat file1.csv file2.csv > target_file.csv

# Create a header file named header.csv
echo "Age,workclass,fnlwgt,education,education-num,marital-status,occupation,relationship,race,sex,capital-gain,capital-loss,hours-per-week,native-country,class" > header.csv

# Add this header file at the beginning of the target_file.cs
cp target_file.csv temp.csv
cat header.csv temp.csv > target_file.csv
rm temp.csv

# Replace ? missing values with your roll number
sed -i "s/?/2018113011/g" target_file.csv
