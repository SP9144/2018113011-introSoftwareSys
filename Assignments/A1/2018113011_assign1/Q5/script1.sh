# Print recursively the last modified date and time and the file name of the all the files in the
# current working directory

for file in $(find .)
do 
    if [ -f $file ]; 
    then
        echo -n $(stat -c %y $file | head -c -17) 
        echo -n " "
        echo $file
    fi
done
echo

# Find all the commands that starts the word “lo” and store the output in a file

 ##compgen -abcdefgjksuv lo > 2018113011.txt  
whatis -r "^lo" > 2018113011.txt

# Display number of lines in above file and length of longest word

echo -n "Number of Lines: "
wc -l 2018113011.txt | cut -d " " -f1

echo -n "Length of Longest Line: "
wc -L 2018113011.txt | cut -d " " -f1

# Replace all occurances of "function" with "method"

    sed -i 's/function/method/g' 2018113011.txt
    #cat 2018113011.txt

# create back-up of above file as backup.txt
    cp 2018113011.txt backup.txt
    #cat backup.txt


