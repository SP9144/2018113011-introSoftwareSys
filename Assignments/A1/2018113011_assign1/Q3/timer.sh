#!/bin/bash
for((i=1; i<=$1; i++))
do 
    echo $i
    if(($i%4==0))
    then 
        notify-send -t 2000 "#$i Work: 25 min"
	echo "#$i Work: 25 min"
        echo -n "Starts at: "
        date +"%H:%M:%S"
        sleep 1s 
        echo -n "Ends at: "
        date +"%H:%M:%S"
        echo

	notify-send -t 2000 "#$i Break: 15 min"
        echo "#$i Break: 15 min"
        echo -n "Starts at: "
        date +"%H:%M:%S"
        sleep 3s 
        echo -n "Ends at: "
        date +"%H:%M:%S"
        echo
        echo
    elif(($i%4!=0))
    then
        notify-send -t 2000 "#$i Work: 25 min"
	echo "#$i Work: 25 min"
        echo -n "Starts at: "
        date +"%H:%M:%S"
        sleep 1s 
        echo -n "Ends at: "
        date +"%H:%M:%S"
        echo
	
	notify-send -t 2000 "#$i Break: 5 min"
        echo "#$i Break: 5 min"
        echo -n "Starts at: "
        date +"%H:%M:%S"
        sleep 1s 
        echo -n "Ends at: "
        date +"%H:%M:%S"
        echo
        echo
    fi
done

echo "Finished"
echo
