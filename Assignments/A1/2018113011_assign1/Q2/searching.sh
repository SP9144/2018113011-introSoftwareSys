#Incorrect number of arguments
if [ $# != 2 ]
then 
    echo "Error: Pls give correct number of arguments."
    exit
fi

#Incorrect URL
wget -q --spider $2
if [ $? != 0 ]
then
    echo "Error: Pls give correct URL."
    exit
else
    echo -n $1
    echo -n " "
    curl -s $2 | sed -e 's/<[^>]*>//g' | grep $1 | wc -l
    #curl -s $2 > temp.txt
    #cat temp.txt 
    #rm temp.txt
fi
