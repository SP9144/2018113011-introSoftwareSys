
echo
echo -n "Enter number of reminders that need to be entered: "
read n

for i in $(seq 1 $n);
do
    echo
    echo "Details of Reminder no.: $i"
    echo
    echo -n "Enter Reminder Name: "
    read name
    echo -n "Enter Reminder Time [in HH:MM format 24 hrs or 12 hrs - support with AM or PM arguments, eg: 8:30 PM or 8:30pm]: "
    read tm
    echo -n "Enter Reminder Date [in month-name day format, eg: Jul 31]: "
    read dt

    if grep -q $name rlist.txt; 
    then
        grep $name rlist.txt
        echo
        echo "Reminder already exists. Enter another one: "
        i=$((i-1))
        continue
    else
        #Appending Reminder to list

        id="$(echo "$name" | cut -c 1-5)$(echo "$tm" | cut -c 1-4)$(echo "$dt" | cut -c 1-4)"
        echo
        echo "Your Reminder has been added to your list."
        echo "$id, $name, $tm, $dt" >> rlist.txt

        #Sending the reminder to be notified

        echo "notify-send \"Reminder\" \"$name\"" | at $tm $dt

    fi 
    

done

echo
echo -n "Do you wish to continue to MENU page (or quit the program)? [Y/n]: "
read ans

if [ "$ans" = "Y" ]
then 
    ./Q4-menu.sh
else
    exit
fi
