
echo
echo -n "Enter id of reminder to be deleted: "
read id
sed -i "/${id}/d" rlist.txt
echo "Deleted reminder $id successfully."
echo

echo
echo -n "Do you wish to continue to MENU page (or quit the program)? [Y/n]: "
read ans

if [ "$ans" = "Y" ]
then 
    ./Q4-menu.sh
else
    exit
fi
