if ! [ -s rlist.txt ]
then
    touch rlist.txt
    echo "REM ID, NAME, TIME, DATE" >> rlist.txt
fi

echo
echo " MENU"
echo " ======================================================================="
echo " 1. Add reminder"
echo " 2. Edit reminder"
echo " 3. Delete reminder"
echo " 4. View all reminders"
echo " 6. Exit"
echo " ======================================================================="
echo
echo -n "--> Pls enter option number: "
read op

case $op in

1) ./Q4-add.sh
    ;;
2) ./Q4-edit.sh
    ;;
3) ./Q4-delete.sh
    ;;
4) ./Q4-view_all.sh
    ;;
5)  exit
    ;;
*)  echo
    echo "Pls enter valid option."
    ./Q4-menu.sh
    ;;

esac

  

