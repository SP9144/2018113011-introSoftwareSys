echo
echo "All Reminders"
echo "=================================================================================================="
echo
column -s "$" -t rlist.txt
echo
echo "=================================================================================================="

echo
echo -n "Do you wish to continue to MENU page (or quit the program)? [Y/n]: "
read ans

if [ "$ans" = "Y" ]
then 
    ./Q4-menu.sh
else
    exit
fi