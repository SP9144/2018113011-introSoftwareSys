
echo -n "Enter Reminder ID for entry to be replaced: "
read id1
sed -i "/${id1}/d" rlist.txt

echo -n "Enter Reminder Name: "
read name
echo -n "Enter Reminder Time [in HH:MM format 24 hrs or 12 hrs - support with AM or PM arguments, eg: 8:30 PM or 8:30pm]: "
read tm
echo -n "Enter Reminder Date [in month-name day format, eg: Jul 31]: "
read dt

id="$(echo "$name" | cut -c 1-5)$(echo "$tm" | cut -c 1-4)$(echo "$dt" | cut -c 1-4)"
echo
echo "Your Reminder has been added to your list."
echo "$id, $name, $tm, $dt" >> rlist.txt

#Sending the remider to be notified
echo "notify-send \"Reminder\" \"$name\"" | at $tm $dt

echo
echo -n "Do you wish to continue to MENU page (or quit the program)? [Y/n]: "
read ans

if [ "$ans" = "Y" ]
then 
    ./Q4-menu.sh
else
    exit
fi