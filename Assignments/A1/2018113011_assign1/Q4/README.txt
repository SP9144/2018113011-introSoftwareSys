++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   ISS
ASSIGNMENT 1
-----------------------------------
Shreeya Pahune
2018113011
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

==================================================================

QUESTION 4: Manage Reminders and Set Desktop Notifications
_________________________

To run script: ./Q4-menu.sh
	
**Pre-requisite: 'at' command must be installed
		 To install : sudo apt-get install at

        This is Menu-driven script which calls other bash scripts to perform the following functions:

        a) Add Entry: Called by ./Q4-add.sh script     
                      Takes input: i) Name i.e. body of reminder
                                   ii) Time 
                                   iii) Date
                      Appends all of the data provided in an entry to the Reminders List
		      Sends Reminder to be notified at given time and date
        
        b) Edit Entry: Called by ./Q4-edit.sh script     
                       Replaces a previous entry with a new one

        c) Delete Entry: Called by ./Q4-delete.sh script     
                         Deletes an entry by Reminder ID given by user

        d) View All Entries: Called by ./Q4-view_all.sh script     
                             Displays all the reminders that are to be notified to the user in a Tabular Format

                            
====================================================================

