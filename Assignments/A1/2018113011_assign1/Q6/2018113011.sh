#!/bin/bash
mkdir $2/Bad
mkdir $2/Average
mkdir $2/Good
mkdir $2/Awesome

$five=5
cat $1 | while read line
do
    #echo "while"
    curr=$(echo $line | head -c -5)  # Gets movie name from movies_input
    #echo $curr
    rating=$(echo $line | tail -c 4)  # Gets rating from movies_input
    rating=${rating%.*}
    #echo $rating
    if [ -n $(find $2 -type f -name "$curr*") ]   # -n to check for null string
    then
        #echo "if1"
	if [[ $rating<5 ]]
	then 
            touch $2/Bad/"$curr".mp4
	elif [[ $rating>=5 && $rating<8 ]]
        then
            touch $2/Average/$curr.mp4
	elif [[ $rating>=8 && $rating<9 ]]
        then
            touch $2/Good/$curr.mp4
	elif [[ $rating>=9 ]]
        then
            touch $2/Awesome/$curr.mp4
        fi
    fi
done
