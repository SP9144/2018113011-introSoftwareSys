awk -F" " '{
    avg = ($1 + $2 + $3);
    if( avg>=80) print $0 ": A";
    else if( avg>=60) print $0 ": B";
    else if( avg>=40) print $0 ": C";
    else print $0 ": F";
    }' marks.txt > grades.txt