
#1(a)

echo "Match “ly”, “rea” or “self”"
echo
cat hamlet.txt | grep 'ly\|rea\|self' --color=auto

#1(b)

echo
echo "Sentence does not begin with "T""
echo
cat hamlet.txt | grep "^[^T]" --color=auto

#1(c)

echo
echo "Contain "s" or any special character (which in this case are ? , - )"
echo
cat hamlet.txt | grep 's\|?\|-\|,' --color=auto

#1(d)

echo
echo "Do not contain the pattern "the", but contains the pattern "to""
echo
cat hamlet.txt | grep -v "the" | grep "to"  --color=auto