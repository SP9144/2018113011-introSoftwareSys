
var data = require('./ISS-lab4/data.js')

function getHighestMarks()
{
    var max=0;
    var name_max;
    for(x in data)
    {
        sum=0
        for (i=0; i<5; i++)
        {
            sum=sum + data[x][i]
        }
        if(sum>max)
        {
            max=sum
            name_max=x
        }
    }
    return [max, name_max]
}

function getSubject2Toppers()
{
    var arr=[];
    for(x in data)
    {
        arr.push({"name": x, "marks": data[x][1]})
    }
    //console.log(arr)
    arr.sort(function(a, b){return a.marks-b.marks})
    arr.reverse()
    return arr
}

console.log(getHighestMarks())
console.log(getSubject2Toppers())

