
function myArray(arr)
{
    this.arr = arr;
}

myArray.prototype.sum = function()
{
    var sum=0;
    for(i=0; i<this.arr.length; i++)
        sum+=this.arr[i];
    return sum;
}

myArray.prototype.product = function()
{
    var pdt=1;
    for(i=0; i<this.arr.length; i++)
        pdt*=this.arr[i];
    return pdt;
}

myArray.prototype.sort = function()
{
    var len = this.arr.length,i, j, t;
    var a=Array.from(this.arr);

    for (i=0; i < len; i++)
    {
        for (j=0; j < len-i; j++)
        {
            if (a[j] > a[j+1])
            {
                t = a[j];
                a[j]=a[j+1];
                a[j+1]=t;
            }
        }
    }
    return a;
}

myArray.prototype.modify = function(x, pos_x)
{
    this.arr[pos_x]=x;
}

myArray.prototype.display = function()
{
    console.log(this.arr);
}

var arr = new myArray([5,4,3,2,1]);

console.log(arr.sum())

console.log(arr.product())

console.log(arr.sort())

arr.modify(0, 2)

arr.display()