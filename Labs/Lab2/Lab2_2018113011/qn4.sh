#!/bin/bash

x=$#

a=("$@")

for((i=0; i<x; i++))
do
    for((j=0; j<x-1; j++))
    do
	if ((${a[$j]}>${a[$((j+1))]}))
	then
	    t=${a[$j]}
	    a[$j]=${a[$((j+1))]}
	    a[$((j+1))]=$t
	fi
    done
done

echo ${a[*]}


