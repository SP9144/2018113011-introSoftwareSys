def JoinIntergers(lst):
    lst = map(lambda x: str(x), lst)
    return int((''.join(lst)))

myList = [int(x) for x in input().split()]

print(JoinIntergers(myList))