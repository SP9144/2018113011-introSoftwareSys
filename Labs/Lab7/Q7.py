import urllib.request
import json

name1 = 'pewdiepie'
name2 = 'tseries'
key = 'AIzaSyDKH9NxXnZ3FU-cHW5NaLekHZRvbQUCi7o'


data1 = urllib.request.urlopen("https://www.googleapis.com/youtube/v3/channels?part=statistics&forUsername="+name1+"&key="+key).read()
data2 = urllib.request.urlopen("https://www.googleapis.com/youtube/v3/channels?part=statistics&forUsername="+name2+"&key="+key).read()

subs1 = json.loads(data1)["items"][0]["statistics"]["subscriberCount"]
subs2 = json.loads(data2)["items"][0]["statistics"]["subscriberCount"]

print(name1 + " has " + "{:,d}".format(int(subs1)) + " subscribers")
print(name2 + " has " + "{:,d}".format(int(subs2)) + " subscribers")
print("The difference is: " + str(int(subs1)-int(subs2)))