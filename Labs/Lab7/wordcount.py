import re
from collections import Counter

try: 
    with open('Q5_textfile.txt', 'r') as f:


        text = re.sub(r'[^\w\s]',' ', f.read())
        text = text.lower().split()

        words = Counter(text)
        
        with open('Q5_output.txt', 'w') as writer:
            for word in words:
                writer.write(word + ': ' + str(words[word]) + '\n')

except FileNotFoundError: 
    print("Error")

